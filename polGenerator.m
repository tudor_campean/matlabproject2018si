function [ g ] = polGenerator( m ,x1,x2)
 
%syms x1 x2 ->pt verificare ca returneaza coeficientii asa cum trebuie

%generating the coeffincients involving only x1^i or x2^i
for i = 1:m

    g(2*i-1) = x1^(i);
    g(2*i) = x2^(i);     
end
g = [1 g];

%generating the coefficinets involving x1^i*x2^j
multiplied = [];
 k = 1;
for i=1:(m-1)
    for j=1:m-i
        
            multiplied(k)=(x1^i*x2^j);
            k = k+1;
        
    end
end

 
 
%putting all the coefficients into one single array
g = [g multiplied];
g;