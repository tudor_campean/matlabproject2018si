function [EMS,ysim ] =ySimGenerator( Y,X1,X2,m )
clear phi_phinal phi;



N = length(X1);
Y_final = reshape(Y,[N^2,1]); %y is now a column vector with dimensions : N^2 1
phi_final = [];

% in this for phi_final is generated, witch is a matrix with all the phi's
% corresponding to each column in y; phi final dimensions: N^2 m
for k = 1:N
    
    %in this for phi is generated for each column in Y; phi dimensions: N m
for i = 1: N

    phi(i,:) = polGenerator(m,X1(k),X2(i)) ;
 
end

    %the new phi is added into the phi_final matrix
    phi_final = [phi_final; phi];
    phi = [];

end

%theta and y simulated are generated
 theta = phi_final\Y_final;
 ysim = phi_final*theta;
 phi_final = [];
 ysim = reshape(ysim,[N,N]);
 
 
 %the EMS for this y simulated is generated
 EMS = 0;
for i=1:N
    EMS = EMS+ sum( (Y(i,:)-ysim(i,:)).^2);
    
end
  EMS = EMS/(N^2);    
 

end

