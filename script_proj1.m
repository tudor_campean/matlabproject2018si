%% Id data set


clear all;
clc;


load('proj_fit_23.mat');

X  =id.X;
X1 = X{1};
X2 = X{2};
Y = id.Y;
%X1=X2


%Premisa:
%X1 -> axa OX ii corespunde indexul i
%X2 -> axa OY ii corespunde indexul j

for i = 1:60
    
m = i;
[EMS,ysim] = simulation(Y,X1,X2,m);

EMS_vector(i) = EMS;

end

EMS_final_index = find( EMS_vector == min(EMS_vector));

[EMS,ysim] = simulation(Y,X1,X2,EMS_final_index);

EMS_final_index
EMS
EMS_vector

 figure, surf(X1,X2,ysim);
 figure,surf(X1,X2,Y);
 
 figure,plot(1:length(EMS_vector),EMS_vector);  % plot EMS for all m's

 
%% Validation data set

clc;
clear all;

load('proj_fit_23.mat');

X  =val.X;
X1 = X{1};
X2 = X{2};
Y = val.Y;
%X1=X2


%Premisa:
%X1 -> axa OX ii corespunde indexul i
%X2 -> axa OY ii corespunde indexul j

for i = 1:50
    
m = i;
[EMS,ysim] = simulation(Y,X1,X2,m);

EMS_vector(i) = EMS;

end

EMS_final_index = find( EMS_vector == min(EMS_vector));

[EMS,ysim] = simulation(Y,X1,X2,EMS_final_index);

EMS_final_index
EMS
EMS_vector

 figure, surf(X1,X2,ysim);
 figure,surf(X1,X2,Y);
 
 figure,plot(1:length(EMS_vector),EMS_vector);  % plot EMS for all m's




%% Verificare polGenerator
 clear all;
 clc;
 syms x1 x2;
load('proj_fit_23.mat');

X  =id.X;
X1 = X{1};
X2 = X{2};
Y = id.Y;
m = 4;
A = polGenerator(m,x1,x2)
 
 %% Verificare simulation
 clear all;
 clc;
 
load('proj_fit_23.mat');

X  =id.X;
X1 = X{1};
X2 = X{2};
Y = id.Y;

 m  =4;
 
 [EMS,ysim ] =simulation( Y,X1,X2,m );
 
 
 surf(X1,X2,ysim);




 
 