function [ MSE_min,MSE_vector,m ] = minMSE( X1,X2,Y ,max_degree)

%obtaining  all the MSE s for the MSE vector
for i = 1:max_degree
 
    m = i;
[MSE,ysim] = ySimGenerator(Y,X1,X2,m);

MSE_vector(i) = MSE;

end

%obtaining the minimum MSE and the value for m for witch  MSE is minimum

MSE_final_index = find( MSE_vector == min(MSE_vector));
m= MSE_final_index;
MSE_min = MSE_vector(m);


end

