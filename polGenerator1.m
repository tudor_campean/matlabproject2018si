function [ poly ] = polGenerator1( deg,x1,x2 )



for i=1:deg
    poly(2*i-1)=x1^i;
    poly(2*i)=x2^i;
end

for i=1:(deg-1)
    for j=1:(deg-1)
        if i+j<=deg
            mult(i,j)=[x1^i*x2^j];
        end
    end
end
mult=reshape(mult,1,[]);
mult(mult==0) = [];
poly=[1,poly,mult];

end

