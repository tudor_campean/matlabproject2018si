clc;
clear all;

load('proj_fit_23.mat');

X  =id.X;
X1 = X{1};
X2 = X{2};
Y = id.Y;
%X1=X2


%Initial assumptions:
%X1 ->  OX axis correspoonds to index  i
%X2 ->  OY axis correspoonds to index j

%obtaining the minimum MSE, the vector with all the MSE's and the y
%simulation for the minimum MSE
[MSE_min,MSE_vector,m_min] = minMSE(X1,X2,Y,4);
[MSE_min_verif,ysim] = ySimGenerator(Y,X1,X2,m_min);


%plot the initial y, the y obtained from the simulation, the evolution of
%MSE when m increases
figure, surf(X1,X2,ysim);
figure,surf(X1,X2,Y);
figure,plot(1:length(MSE_vector),MSE_vector);  % plot EMS for all m's

%%

 clc;
clear all;

load('proj_fit_23.mat');

X  =val.X;
X1 = X{1};
X2 = X{2};
Y = val.Y;
%X1=X2


%Initial assumptions:
%X1 ->  OX axis correspoonds to index  i
%X2 ->  OY axis correspoonds to index j

%obtaining the minimum MSE, the vector with all the MSE's and the y
%simulation for the minimum MSE
[MSE_min,MSE_vector,m_min] = minMSE(X1,X2,Y,4);
[MSE_min_verif,ysim] = ySimGenerator(Y,X1,X2,m_min);

%plot the initial y, the y obtained from the simulation, the evolution of
%MSE when m increases
figure, surf(X1,X2,ysim);
figure,surf(X1,X2,Y);
figure,plot(1:length(MSE_vector),MSE_vector);  % plot EMS for all m's



